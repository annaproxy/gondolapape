pape=$1
which=${2:-arch}
width=$(magick convert "$pape" -format "%wx%h" info:)
magick convert "$which.png" -gravity center \
    -background None -extent $width "tempgond.png"
magick "$pape" "tempgond.png" -flatten "$pape-$which-gondola.png"
rm "tempgond.png"